#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#define SIZE 4096
#define BLOCK_SIZE 100

void success(int test_number) { printf("Test %d passed\n", test_number); }

void TEST_HEAP() {
    struct region *heap = heap_init(SIZE);
    assert(heap);
    heap_term();
    success(1);
}
void TEST_BLOCKS() {
    heap_init(SIZE);
    void *block_1 = _malloc(BLOCK_SIZE);
    void *block_2 = _malloc(BLOCK_SIZE);
    void *block_3 = _malloc(BLOCK_SIZE);
    _free(block_1);
    assert(block_2);
    assert(block_3);
    success(2);
    _free(block_2);
    assert(block_3);
    success(3);
}
void TEST_EXPANSION() {
    struct region *heap = heap_init(0);
    assert(heap);
    _malloc(BLOCK_SIZE);
    assert(heap->size == BLOCK_SIZE);
    success(4);
}
void TEST_EXPANSION_MOVED() {
    void *heap = heap_init(0);
    void *lock = heap + SIZE;
    void *block1 = _malloc(SIZE);
    void *block2 = _malloc(SIZE);
    assert(block1);
    assert(block2);
    _free(block1);
    _free(block2);
    _free(lock);
    assert(heap);
    success(5);
}

int main() {
    TEST_HEAP();
    TEST_BLOCKS();
    TEST_EXPANSION();
    TEST_EXPANSION_MOVED();
    return 0;
}